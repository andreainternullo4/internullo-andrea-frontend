import Carousel from "./Components/Carousel/Carousel"
import ContainerBand from "./Components/ContainerBand/ContainerBand"
import ContainerTourdates from "./Components/ContainerTourdates/ContainerTourdates"
import Contatti from "./Components/Contatti/Contatti"
import Nav from "./Components/Navbar/Navbar"
import Footer from "./Components/Footer/Footer"

function App() {

  return (
    <>
      <header>
        <Nav></Nav>
        <Carousel></Carousel>
      </header>
      
        <ContainerBand></ContainerBand>
        <ContainerTourdates></ContainerTourdates>
        <Contatti></Contatti>
        <Footer></Footer>
    </>
  )
}

export default App
