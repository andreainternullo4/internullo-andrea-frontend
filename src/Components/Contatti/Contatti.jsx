import "./style.css"

export default function Contatti() {
    return(
        <div className="container text-center contatti" id="contact" style={{marginTop: '10%'}}>
				<div className="row justify-content-center recapiti">
					<div className="col col-lg-2 justify-content-center">
						<p>Fan? Drop a note.</p>
						<p><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-3.148 0-6 2.553-6 5.702 0 3.148 2.602 6.907 6 12.298 3.398-5.391 6-9.15 6-12.298 0-3.149-2.851-5.702-6-5.702zm0 8c-1.105 0-2-.895-2-2s.895-2 2-2 2 .895 2 2-.895 2-2 2zm4 14.5c0 .828-1.79 1.5-4 1.5s-4-.672-4-1.5 1.79-1.5 4-1.5 4 .672 4 1.5z"/></svg>Chicago, US</p>
						<p><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20 2c0-1.105-.896-2-2-2h-12c-1.105 0-2 .896-2 2v20c0 1.104.895 2 2 2h12c1.104 0 2-.896 2-2v-20zm-9.501 0h3.001c.276 0 .5.224.5.5s-.224.5-.5.5h-3.001c-.275 0-.499-.224-.499-.5s.224-.5.499-.5zm1.501 20c-.553 0-1-.448-1-1s.447-1 1-1c.552 0 .999.448.999 1s-.447 1-.999 1zm6-3h-12v-14.024h12v14.024z"/></svg>Phone: +00 15151515</p>
						<p><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm22 16l-6.526-6.618-3.445 3.483-3.418-3.525-6.611 6.66 5.051-8-5.051-6 10.029 7.446 9.971-7.446-4.998 6.01 4.998 7.99z"/></svg>Email: mail@mail.com</p>
					</div>
					<div className="col col-lg-4 justify-content-center campi">
						<form className="text-center">
							<fieldset>
								<div>
									<label for="Nome"></label>
									<input type="text" placeholder="Nome"/>
								</div>
								<div>
									<label for="Email"></label>
									<input type="text" placeholder="Email"/>
								</div>
								<div className="desc">
									<label for="Descrizione"></label>
									<textarea cols="49" rows="" type="text" placeholder="Descrizione"></textarea>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
    );
}