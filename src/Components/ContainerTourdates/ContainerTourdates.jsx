import "./style.css"
import newyork from "../../assets/newyork.jpg"
import roma from "../../assets/roma.jpg"
import parigi from "../../assets/parigi.jpg"

export default function ContainerTourdates() {
    return(
        <div className="container-fluid tourdates bg-dark" id="tour">				
				<div className="listaDate">
					<h4>TOUR DATES</h4>
					<span className="italic">Lorem ipsum we'll play some music</span>
					<span className="italic">Remember to book your tckets!</span>
					
					<ul>
						<li style={{textAlign: 'left'}}>
							<p>September</p>
						</li>
						<li style={{textAlign: 'left'}}>
							<p>October</p>
						</li>
						<li style={{textAlign: 'left'}}>
							<p>November</p>
						</li>
					</ul>
				</div>
				<div className="container-fluid" style={{textAlign: 'center'}}>
					<div className="row justify-content-md-center">
						<div className="col col-md-3 justify-content-center"> 
							<div className="card text-center data1">
								<img src={newyork} className="card-img-top" alt="New York" />
								<div className="card-body">
									<h5 className="card-title">New York</h5>
    								<p className="card-text">Friday 27 Novembre, 2015</p>
    								<a href="#" className="btn btn-dark">Buy ticket</a>
								</div>
							</div>
						</div>
						<div className="col col-md-3 justify-content-center"> 
							<div className="card text-center data1">
								<img src={roma} className="card-img-top" alt="Roma" />
								<div className="card-body">
									<h5 className="card-title">Roma</h5>
    								<p className="card-text">Friday 27 Novembre, 2015</p>
    								<a href="#" className="btn btn-dark">Buy ticket</a>
								</div>
							</div>
						</div>
						<div className="col col-md-3 justify-content-center"> 
							<div className="card text-center data1">
								<img src={parigi} className="card-img-top" alt="Parigi" />
								<div className="card-body">
									<h5 className="card-title">Parigi</h5>
    								<p className="card-text">Friday 27 Novembre, 2015</p>
    								<a href="#" className="btn btn-dark">Buy ticket</a>
								</div>
							</div>
						</div>
					</div>				
				</div>
			</div>
    );
}