import newyork from "../../assets/newyork.jpg"
import parigi from "../../assets/parigi.jpg"
import roma from "../../assets/roma.jpg"

export default function Carousel() {
    return(
        <div id="carouselExampleCaptions" className="carousel slide">
  			<div className="carousel-indicators">
    			<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    			<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    			<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  			</div>
  			<div className="carousel-inner">
    			<div className="carousel-item active">
      				<img src={newyork} className="d-block w-100" alt="New York" />
      				<div className="carousel-caption d-none d-md-block">
        				<h5>New York</h5>
        				<p>Some representative placeholder content for the first slide.</p>
      				</div>
    			</div>
    			<div className="carousel-item">
      				<img src={roma} class="d-block w-100" alt="Roma" />
      				<div className="carousel-caption d-none d-md-block">
        				<h5>Roma</h5>
        				<p>Some representative placeholder content for the second slide.</p>
      				</div>
   				</div>
    			<div className="carousel-item">
      				<img src={parigi} className="d-block w-100" alt="Barcellona" />
      				<div className="carousel-caption d-none d-md-block">
        				<h5>Parigi</h5>
        				<p>Some representative placeholder content for the third slide.</p>
      				</div>
   				</div>
  			</div>
  			<button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    			<span className="carousel-control-prev-icon" aria-hidden="true"></span>
    			<span className="visually-hidden">Previous</span>
  			</button>
  			<button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    			<span className="carousel-control-next-icon" aria-hidden="true"></span>
    			<span className="visually-hidden">Next</span>
  			</button>
		</div>
    );
}