import "./style.css"
import newyork from "../../assets/newyork.jpg"
import roma from "../../assets/roma.jpg"
import parigi from "../../assets/parigi.jpg"

export default function ContainerBand() {
    return(
        <div className="container-fluid theband" id="band">
				<h4>THE BAND</h4>
				<span className="italic">We love music</span>
				<p>Morbi blandit eu leo sed imperdiet. Quisque feugiat, urna id tempus sodales, quam eros commodo lectus, non efficitur ante nisl nec justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Maecenas non blandit tellus, eu varius ipsum. Phasellus libero arcu, facilisis at velit non, pretium feugiat massa. Integer ac efficitur urna. Fusce diam nunc, mollis non tellus in, faucibus condimentum nulla. Nullam quis venenatis libero, sit amet hendrerit dui. Praesent tristique mauris at quam auctor, vel commodo metus vulputate. Duis sapien ante, dignissim in sapien sit amet, commodo sagittis mi. Fusce commodo imperdiet eros id placerat. Maecenas dictum metus vitae porta tincidunt. Nulla facilisi. Cras sit amet egestas massa, a volutpat nibh. Donec varius rhoncus gravida. In hac habitasse platea dictumst.</p>
				
				<div className=" container-fluid selettoreCitta">
					<div className="row justify-content-md-center">
						<div className="col col-md-3 justify-content-center">
							<div className="card">
  								<img src={newyork} className="card-img-top" alt="New York" />
  								<div className="card-body">
    								<p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  								</div>
							</div>
						</div>
						<div className="col col-md-3 justify-content-center">
							<div className="card">
  								<img src={roma} className="card-img-top" alt="Roma" />
  								<div className="card-body">
    								<p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  								</div>
							</div>
						</div>
						<div className="col col-md-3 justify-content-center">
							<div className="card">
  								<img src={parigi} className="card-img-top" alt="Parigi" />
  								<div className="card-body">
    								<p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    )
}