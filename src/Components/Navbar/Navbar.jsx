import "./style.css"

export default function Nav() {

    return(
            <>
                <nav className="navbar navbar-expand-lg fixed-top bg-dark" id="nav">
  			        <div className="container-fluid">
    		            <a className="navbar-brand" href="#">TESTInternullo</a>
    		            <button className="navbar-toggler bg-white" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      			            <span className="navbar-toggler-icon"></span>
    		            </button>
    			            <div className="collapse navbar-collapse" id="navbarNavDropdown">
      				            <ul className="navbar-nav">
        				            <li className="nav-item">
         				 	            <a className="nav-link" aria-current="page" href="#">Home</a>
        				            </li>
        				            <li className="nav-item">
          					            <a className="nav-link" href="#band">Band</a>
        				            </li>
        				            <li className="nav-item">
          					            <a className="nav-link" href="#tour">Tour</a>
        				            </li>
        				            <li className="nav-item">
          					            <a className="nav-link" href="#contact">Contact</a>
        				            </li>
        				            <li className="nav-item dropdown">
          					            <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            					            More
          					            </a>
          					            <ul className="dropdown-menu">
            					            <li><a className="dropdown-item" href="#">Action</a></li>
            					            <li><a className="dropdown-item" href="#">Another action</a></li>
								            <li><a className="dropdown-item" href="#">Something else here</a></li>
          					            </ul>
        				            </li>
      				            </ul>
    			            </div>
  			        </div>
		        </nav>
            </>
    );
}